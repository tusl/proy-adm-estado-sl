# NOTAS

Los gobiernos de la región se encuentran desarrollando planes de modernización que incluyen el uso intensivo de las tecnologías de la información y las comunicaciones. El propósito es por un lado, mejorar la calidad del servicio público, y por el otro, facilitar el acceso de la comunidad a la administración pública, tanto a la información, como a la posibilidad de realizar trámites, y en los casos más avanzados, a permitir la participación democrática utilizando el voto electrónico.

En ese sentido, los países han desarrollado políticas públicas que, por un lado, intentan superar la brecha digital y por el otro, aumentan la efectividad de la gestión administrativa pública. Nuestro país no ha sido ajeno a esta evolución

Dice CASTELLS: “El Estado red es el Estado de la era de la información, la forma política que permite la gestión cotidiana de la tensión entre lo local y lo global ... es el principal instrumento del que hoy por hoy disponen los ciudadanos para controlar la globalización en función de sus valores e intereses. Por ello la adecuación del instrumento, la administración del Estado, a las tareas complejas que requiere el extraordinario proceso de cambio social y tecnológico que vivimos, es la condición previa a cualquier capacidad de intervención estratégica pública, a cualquier reforma social.”

En tal sentido, Castells sintetiza la construcción del Estado – red como un fenómeno que se da en torno a la combinación de ocho principios estructurantes de la administración pública:

a) SUBSIDIARIEDAD : la gestión debe situarse, para cada problema o tarea, en el ámbito más descentralizado posible en donde pueda desempeñarse eficazmente;

b) FLEXIBILIDAD : las estructuras organizacionales deben tener capacidad para adaptarse a las necesidades de intervención del Estado;

c) COORDINACIÓN : en cuanto a la capacidad de cooperación entre los diversos componentes del Estado;

d) PARTICIPACIÓN CIUDADANA : para garantizar la legitimidad en los procesos de toma de decisión pública;

e) TRANSPARENCIA : de modo tal que quede garantizado el acceso a la información por parte de la ciudadanía;

f) MODERNIZACIÓN TECNOLÓGICA : a través del uso constante de las redes informáticas y de las comunicaciones;

g) TRANSFORMACIÓN DE LOS AGENTES DE LA ADMINISTRACIÓN : mediante su profesionalización, jerarquización y revalorización, y;

h) RETROACCIÓN EN LA GESTIÓN : como mecanismo de autoaprendizaje al interior de las organizaciones.
