## Proyectos para la administración del Estado con SL

Lineamientos:

- Los procesos de análisis de demanda de software.
- Procesos de selección de SL.
- Viabilidad funcional, operativa y técnica.
- Toma de decisiones sobre las áreas estratégicas de desarrollo de SL.
- El caso de los SIG.