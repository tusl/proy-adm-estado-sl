.. header:: 
    PAESL - Unidad 2 - Procesos de análisis de la demanda tecnológica - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 
..    SetPageCounter 1 arabic


Análisis de la demanda tecnológica de la administración pública
===============================================================

En la administración pública la demanda de soluciones tecnológicas para procesos administrativos es constante. La variedad de procesos asociadas a los servicios que gestiona el Estado, tanto internamente para la interrelación entre sus dependencias como los servicios brindados a otros organismos, estatales y privados, y la ciudadanos en general, exige un permanente análisis para determinar las necesidades que requieren soluciones tecnológicas, tanto de infraestructura como de implementación y/o desarrollo de software.

En este marco es que se pretende delinear una serie de preceptos para estandarizar el proceso de análisis y sistematizar de alguna manera la selección de la mejor opción para dar solución a las demandas planteadas.

La implementación de tecnología en la administración pública, en cualquiera de sus niveles, comunal o municipal, provincial o nacional, es heterogéneo y diverso. Cada dependecia define una línea de trabajo que muchas veces no está pensada a largo plazo sino a etapas cumplibles en tiempos de la gestión política de gobierno.

De acuerdo al nivel de gobierno y del presupuesto disponible existen varias alternativas. 

Los niveles provinciales, nacionales y algunos municipios grandes, tienen generalmente áreas de tecnología que llevan a cabo el desarrollo y la implementación de software y hardware en el marco de proyectos definidos en la línea política definida. Esto permite una integración de sistemas e intercambio de información entre las dependencias de la administración pública.

En el otro extremo, existen administraciones con niveles de informatización precarios y que carecen de recursos, tanto humanos como técnicos, para llevar adelante proyectos de tecnología.

En estos casos existen dos posibles líneas de acción, aquellas administraciones que tiene presupuesto y resuelven la demanda de soluciones tecnológicas tercerizando el desarrollo y mantenimiento del software o recurriendo a la compra de software *enlatado*.

Las iniciativas de mejora o implementación de proyectos en la administración pública están relacionados con:

- La planificación adecuada de los proyectos considerando las características propias del sector público.
- La administracion de riesgos asociados.
- El diseño de contratos y licitaciones, en caso de necesitarse, más flexibles.
- Una mejor comprensión del negocio por parte de los directivos informáticos.
- La administración de cambios durante el desarrollo del proyecto.
- La gestión de los proveedores de tecnología.
- La conformación de equipos de trabajos competentes.
- El diseño de la arquitectura tecnológica.


Estructura de un proyecto
-------------------------

El ciclo de vida de un proyecto se realiza en tres etapas generales: el **diseño**, la **ejecución** y la **operación**.

Existen, también, factores de riesgo en lo que al desempeño del proyecto se refiere, y se relacionan con tres elementos importantes, que son, El **tamaño del proyecto**, la **calidad de la definición de requerimientos** de la demanda y el **nivel de conocimiento de la tecnología necesaria** para resolver esa demanda.


Proyectos de TICs en el Estado
------------------------------

En la etapa temprana, diseño y análisis, de todo proyecto en el sector público, aparecen cuestiones propias del ámbito estatal que, al no prensentarse en el estudio de los proyectos del ámbito privado, resulta difícil entender y desarrollar.

Estos factores están asociados a problemáticas propias del sector público y tienen que ver con el tipo de tecnología que se quiere implementar y fundamentalmente a factores de gestión.


Gobierno
++++++++

Los actos y gastos de gobierno están bajo el escrutinio público exigiendo un componente importante de difusión del proyecto, actividad que no necesariamente se encuentran evaluada ni dimensionada.

Otro factor de este nivel son los tiempos políticos que en la mayoría de los casos juegan en contra de los tiempos reales de desarrollo e implementación. Los proyectos se promocionan antes de su puesta en marcha lo que afecta desde el punto de vista de la promesa y las expectativas asociadas al resultado. 

Los cambios en las prioridades de gobierno implica en ciertas circunstancias la merma en los recursos asignados a los proyectos.

La existencia de un marco regulatorio más rígido afecta en situaciones en las cuales el proyecto debe rediseñarse y esto no es posible producto del marco jurídico.

El requerimiento, en muchos casos, de una coordinación entre instituciones incorpora nuevas complejidades ya que se requiere de una visión y compromiso que va más allá de la institución que encara el proyecto.


Tecnología
++++++++++

La velocidad con que se producen los cambios tecnológicos hace que la solución llegue con retardo debido a la lentitud del Estado en los procesos de gestión de proyectos.

En el caso de los estados en los que se cuenta con recursos humanos propios para la gestión de proyectos de TICs, la complejidad se manifiesta en las políticas de desarrollo de proyectos y la capacitación del personal acorde a esa política.

En general los proyectos tecnológicos dentro del Estado tienen alta complejidad debido a los niveles de integración y volúmenes asociados, gran cantidad de transacciones, usuarios y/o volúmenes de datos.

El nivel de desarrollo de TICs en los ámbitos estatales es muy heterogéneo. Se pueden apreciar instituciones con un
gran desarrollo habitualmente cercanas al gobierno central, asociadas a los organismos del tipo hacienda o economía (por el presupuesto que administran) y otras, con un bajo nivel de desarrollo, tal es el caso de organismos más chicos dentro de la organización estatal, algunos municipios y servicios públicos pequeños. 


Motivos de Fracasos de Proyectos de TICs
----------------------------------------

Al mirar el comportamiento de los proyectos TIC, aparecen algunos elementos que se repiten y que en general pueden asociarse al fracaso de los mismos. Estos elementos van a estar presentes en mayor o menor medida dependiendo de la madurez de la institución y de los equipos de proyecto que pueda conformar.

Entre las principales causas de fracasos de los proyectos se pueden mencionar:

- Falta de vínculo entre el proyecto y las prioridades estratégicas de la institución 
- Falta de liderazgo y *ownership* del proyecto
- Falta de habilidades de gestión de proyectos y administración del riesgo 
- Poco conocimiento de la industria TI y de los proveedores
- Evaluación de propuestas con mirada de corto plazo sustentado en oferta económica y no en valor del gasto 
- Pocas iniciativas para segmentar los proyectos en tamaños más manejables
- Arquitectura tecnológica mal definida

Una herramienta que puede ayudar es una pauta de evaluación desarrollada por la *Parliamentary Office of Science and Technology (Government IT Projects)* en el Reino Unido, cuya finalidad es evaluar un proyecto en particular sobre la base de factores de éxito y fracaso para dieciséis áreas, las cuales pueden determinar la evolución de un determinado proyecto de TICs:


.. list-table:: Parliamentary Office of Science and Technology (Government IT Projects)
   :widths: 35 25 35
   :header-rows: 1

   * - Factores de Éxito
     - Elementos
     - Factores de Fracaso
   * - + Corta y realista
       + Dividida en fases
       + Flexible
     - Escala de Tiempo
     - + Larga
       + Corta de forma poco realista


   * - + Procesos sólidos
       + Compromiso mutuo
       + Decisiones rápidas
     - Aprobación y Aceptación
     - + Procesos no formales
       + Dilación
       + Falta de autoridad delegada


   * - + Bien especificados
       + Criterios de aceptación claros
       + Bajo control de la administración del proyecto
     - Requerimientos
     - + Alto Nivel
       + Ambigüedad y fluidez
       + Abiertos a interpretación


   * - + Flexible
       + Planes de contingencia en posición
       + Pago asociado a entregables
     - Presupuesto
     - + Fragmentario
       + Reducido
       + Sin planes d econtingencia


   * - + Personal capacitado disponible
       + Con experiencia y entrenamiento
       + Estructura de poca profundidad
     - Dirección de Proyectos
     - + Competenciasno disponibles
       + Sobrecargados o sin experiencia
       + Estructura demasiado jerárquica


   * - + Totalmente comprometido, responsable
       + Atención a los detalles
       + Orientado aléxito
     - Actitud de Negocios
     - + Falta de interés
       + Hostilidad ante malas noticias
       + *Se debe ganar* a cualquier costo


   * - + Completos
       + Bien definidos
       + Métrica relevante
     - Objetivos
     - + Incompletos
       + Vagos
       + Sin métrica


   * - + Aplicado desde el inicio
       + Evaluación frecuente
       + Planes de contingencia
     - Manejo de Riesgo
     - + Ignorado
       + Intermitente
       + Sin planes de contingencia


   * - + Firmemente aplicado
       + Bien documentado
     - Control de Cambio
     - + Perdido
       + No documentado


   * - + Cercana
       + Partnering
       + Buena comunicación
     - Relación con Proveedores
     - + Adversa
       + Comunicación escasa


   * - + Orientado al éxito
       + Estable
       + Calificado y con experiencia PM
     - Equipo de Proyecto
     - + Orientado al procedimiento
       + Volátil
       + Administración difusa


   * - + Cuidadosas
       + Criterios totalmente documentados
       + Pre y post integración
       + Participación del usuario
     - Pruebas
     - + Incompletas
       + Sin benchmark
       + Sin participación del usuario


   * - + Integrado en el equipo
       + Posee los requerimientos
       + Involucrado a lo largo del proyecto
       + Conoce estructura del proyecto
     - Participación del Usuario
     - + No incluido en equipo de proyecto
       + Sólo involucrado después de la entrega


   * - + Planificado desde el inicio
       + Involucra a Usuarios
       + Con seguimiento
     - Entrenamiento
     - + Al final del desarrollo
       + Realizado sólo por Proveedor
       + Incompleto


   * - + Soluciones establecidas
       + Expectativas reales
       + No complejo
     - Tecnología
     - + Conduce el proyecto
       + No madura


   * - + Modular
       + Basado en software estándar
     - Diseño
     - + Monolítico y rígido
       + Implementación big-bang



Iniciativas de Mejora
---------------------

Al analizar el comportamiento de diferentes proyectos TIC, se visualizan algunas iniciativas que pueden adoptar los gestores públicos, que les permitirán reducir los riesgos asociados a los proyectos.

Algunas de las prácticas que aportan a reducir los riesgos asociados de los proyectos son:

Planificación del proyecto
  Establecer una adecuada planificación, lo que implica un diseño de todos sus componentes, entre las que podemos mencionar como prerequisitos mínimos, diseño técnico de la solución, estructura de los equipos de trabajo con sus roles y responsabilidades, entregables finales e intermedios, tiempos asociados a su desarrollo/implementación y finalmente los costos involucrados. Estas labores debiera abordar la oficina de proyectos.


Administración de Riesgos
  Identificar los riesgos potenciales del proyecto en sus diferentes áreas (técnica, organizacional, económica y de proyecto) y definir estrategias de mitigación en los escenarios evaluados.


Diseño Contractual (sólo en el caso de proveedores externos)
  Dependiendo del modelo y profundidad de la externalización del proyecto, el diseño contractual puede tomar una preponderancia significativa.

  El diseño del contrato debe evaluar las complejidades del servicio, los mecanismos para su medición o niveles de servicio (SLA) asociado, el esquema de administración del contrato, gestión de cambios, propiedad intelectual, mecanismos de delimitación de responsabilidades y finalmente modelos de incentivos y multas.

  Es deseable que se evalúe la evolución que puede tener el servicio/producto en el tiempo de forma tal de prever esquemas de modificación contractual.


Entendimiento del Negocio
  Todo proyecto tecnológico se encuentra en un contexto de negocio y por lo tanto debe diseñarse y ejecutarse sin perder de vista aquello. Es muy frecuente que administradores de tecnología miren más a la tecnología en si misma que al aporte o valor agregado que ella entrega a los procesos.

  Es muy común ver en proyectos tecnológicos en el estado que al final del día lo que queda es infraestructura (servidores, sistemas de almacenamiento, comunicaciones, etc.) pero no hay un cambio real de prácticas.


Administrar Cambios
  En la mayoría de los proyectos TIC y sobretodo en aquellos que van más allá de la adquisición de infraestructura se producirán cambios desde el diseño original hasta la solución final.

  Lo anterior no es malo, de hecho es un reflejo de la realidad. El desafío reside en la forma que se administran esos cambios, como se evalúan, se priorizan y finalmente se incorporan en la solución final.

  Al observar las principales metodologías y prácticas del mundo de las TICs (CMMi, ITIL y otras), éstas consideran mecanismos de gestión de cambios como una componente muy relevante y que debe ser administrada. En el ámbito público resulta razonable provisionar tiempo y recursos económicos en función de potenciales cambios que se puedan producir.


Relaciones con los proveedores
  El Estado tiende a establecer relaciones esquizofrénicas con sus proveedores, el proveedor siente que el Estado quiere exprimirlo hasta el final y el Estado siente que no va a recibir por lo que está pagando. Una forma de resolver esto es establecer relaciones de confianza y largo plazo sin descuidar las restricciones existentes en los marcos jurídicos (legislación de compras públicas).

  Algunas experiencias exitosas de mejoramiento de relaciones son convenios marco de servicios, regulación de la gestión de cambios ex-ante y mecanismos de incentivos y multas adecuados.

  Otro antecedente importante es que el mercado de las TICs en muchos países está basado en empresas pequeñas y medianas, tal es el caso de las empresas desarrolladoras de software, de ingeniería de sistemas y soporte. La práctica tradicional de muchas instituciones públicas es contratar a grandes empresas tecnológicas, lo que termina ocurriendo es que estas se transforman en intermediarios de pequeñas empresas. La forma de resolver esto es bajando las barreras de acceso a dicho segmento.


Equipo de Trabajo
  Dependiendo del tipo de proyecto y de los niveles de externalización que este tenga, serán los perfiles profesionales requeridos.  Por lo que cabe señalar que una evaluación ex-ante del equipo requerido (interno y externo) y de las brechas existentes en términos de competencias de forma de tomar las acciones necesarias (contratación, capacitación, coaching).

  Dentro del equipo de trabajo un rol fundamental es el del Gerente del Proyecto. En algunas instituciones públicas buscan reducir costos asociados a este perfil, en proyectos de gran envergadura el costo asociado a la administración del mismo es insignificante y el aporte que se hace contando con un buen administrador de proyectos a lograr un proyecto existoso y un mejor valor del gasto es muy importante.


Arquitectura Tecnológica
  Todo proyecto TIC requiere de una arquitectura tecnológica con sus diversas componentes: física, lógica y de interoperabilidad entre otros.

  Muchos estados se encuentran trabajando e nivel estatal en el diseño de una arquitectura que de cuenta de las funciones requeridas, stack de productos de software básico y el modelo de interoperabilidad dentro del Estado con el objeto de mapear el estado del arte y su evolución futura.
  
  Gran parte de los fracasos en proyectos de TICs se ha debido a un diseño arquitectónico pobre o de corto alcance.



Por otra parte se pueden desarrollar iniciativas de carácter más colectivo que pueden ayudar al mejoramiento de los proyectos:

Compartir experiencias
  Establecer prácticas formales de intercambio de experiencias de gestión e implementación de proyectos para parte de ejecutivos de TICs del Estado.

Capacitación
  Desarrollar programas de capacitación y certificación de ejecutivos de TICs dentro del estado.

Centros de Competencia
  Establecer centro de competencia, que permitan ser un punto de apoyo a aquellas instituciones más débiles en el ámbito de proyectos. Este tipo de iniciativas pueden desarrollar herramientas de diseño y monitores de proyectos, en particular en sus etapas más tempranas.



.. Cierre de apunte con bibliografía

.. raw:: pdf

    PageBreak

Bibliografía
============

.. raw:: pdf

    Spacer 0 10

* Proyectos TIC en el Sector Público. Alejandro Barros. (`http://www.alejandrobarros.com/proyectos-tic-en-el-sector-publico/ <http://www.alejandrobarros.com/proyectos-tic-en-el-sector-publico/>`_)




