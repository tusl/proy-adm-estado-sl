# Cuestionartio Unidades 1 y 2

## Pregunta 1:

Seg?n la *RAE*, El estado es:

1. Una organizaci?n civil, dotada de poder absoluto y total, que integra la poblaci?n de una naci?n.
2. Una organizaci?n pol?tica, dotada de poder soberano e indepediente, que integra la poblaci?n de un territorio.
3. Un conjunto de ciudadanos que delegan poder en un grupo de personas para que administre los recursos del pa?s.



## Pregunta 2:

Max Weber, en 1919, define el Estado moderno como:

?Una asociaci?n de dominaci?n con car?cter institucional que ha tratado, con ?xito, demonopolizar dentro de un territorio la violencia f?sica leg?tima como medio de dominaci?n y que, con este fin, ha reunido todos los medios materiales en manos de sus dirigentes y ha expropiado a todos los seres humanos que antes dispon?an de ellos por derecho propio, sustituy?ndolos con sus propias jerarqu?as supremas?.

Por ello se hallan dentro del estado instituciones tales como:

1. Las fuerzas armadas, la administraci?n p?blica, los tribunales y la polic?a.
2. Las fuerzas armadas, las asociaciones civiles, los tribunales y clubes deportivos.
3. Las fuerzas armadas, la administraci?n p?blica, las ONGs y las instituciones religiosas.



### Pregunta 3:

