.. header:: 
    PAESL - Unidad N - Conceptos de Estado y su funcionamiento - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 
..    SetPageCounter 1 arabic


TITULO PRINCIPAL
================




.. Cierre de apunte con bibliografía

.. raw:: pdf

    PageBreak

Bibliografía
============

* Páginas de Manual (Man Pages) del sistema operativo.

.. raw:: pdf

    Spacer 0 10

La bibliografía que se indica a continuación corresponde a material que provee una visión interesante sobre los temas propuestos en esta unidad. Es documentación más completa e incluso más extensiva en el desarrollo de algunos temas.

* The Debian Administrator's Handbook, Raphaël Hertzog and Roland Mas, (`https://debian-handbook.info/ <https://debian-handbook.info/>`_)

* `Administración de sistemas GNU/Linux <http://ocw.uoc.edu/informatica-tecnologia-y-multimedia/administracion-de-sistemas-gnu-linux-1>`_, Máster universitario en Software Libre, Universitat Oberta de Catalunya

* Básicamente GNU/Linux, Antonio Perpiñan, Fundación Código Libre Dominicano (`http://www.codigolibre.org <http://www.codigolibre.org>`_)


