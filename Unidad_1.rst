.. HEADER:: 
    PAESL - Unidad 1 - Conceptos de Estado y soberanía tecnológica - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. role:: bash(code)
    :language: bash

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 200
    PageBreak


:Autor:       Leonardo Martinez  

.. 
    :Colaborador: 
    :Colaborador: 
    :Colaborador: 

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución Compartir Igual (CC-BY-SA) 4.0 Internacional. `<http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. raw:: pdf

    Spacer 0 20

.. image:: imagenes/licencia.png 

.. raw:: pdf

    PageBreak 
..    SetPageCounter 1 arabic


Introducción
============

Con el contenido de esta materia se pretende introducir conceptos que sirvan al técnico para la evaluación, análisis, desarrollo e implementación de proyectos de tecnología en el ámbito de la administración pública del Estado. Para ello se necesita conocer previamente algunos conceptos básicos de la gestión de gobierno. Entender qué es un Estado y cómo se gestiona, su funcionamiento, niveles de gobierno y sus alcances.

El objetivo de la utilización de software libre para la definición de proyectos para administrar el Estado está en el sentido de generar soluciones para una gestión de la información estatal que permita avanzar en la transparencia de los actos administrativos logrando afianzar la soberanía tecnológica.


Estado
======

Las sociedades humanas, desde que se tiene noticia, se han organizado políticamente. Tal organización puede llamarse **Estado**, en tanto y en cuanto corresponde a la agregación de personas y territorio en torno a una autoridad, no siendo, sin embargo, acertado entender la noción de estado como única y permanente a través de la historia.

El *Estado* [#]_ ,de acuerdo a la definición de la Real Academia Española, es una *organización política*, dotada de poder soberano e independiente, que integra la población de un territorio. Hace referencia a la organización social, política, coercitiva y económica, conformada por un conjunto de instituciones que tienen el poder de regular la vida en sociedad de un país.

Como término tiene muchas acepciones, designa también a todo aquel país soberano, reconocido como tal en el orden internacional, así como al conjunto de poderes y órganos de gobierno de dicho país.​

.. [#] Diccionario de la Real Academia Española. `Estado. acepciones 6 y 7.`_

.. _Estado. acepciones 6 y 7.: http://dle.rae.es/?id=GjqhajH


El concepto de Estado difiere según los autores, pero algunos de ellos definen el Estado como el conjunto de instituciones que poseen la autoridad y potestad para establecer las normas que regulan una sociedad, teniendo soberanía interna y externa sobre un territorio determinado. *Max Weber*, en 1919, define el Estado moderno como:

.. class :: blockquote

«Una asociación de dominación con carácter institucional que ha tratado, con éxito, de monopolizar dentro de un territorio la violencia física legítima como medio de dominación y que, con este fin, ha reunido todos los medios materiales en manos de sus dirigentes y ha expropiado a todos los seres humanos que antes disponían de ellos por derecho propio, sustituyéndolos con sus propias jerarquías supremas».
    
Por ello se hallan dentro del Estado instituciones tales como las fuerzas armadas, la administración pública, los tribunales y la policía, asumiendo pues el Estado las funciones de defensa, gobernación, justicia, seguridad y otras, como las relaciones exteriores.

De una manera general, entonces, puede definirse al Estado como la organización en la que confluyen cuatro elementos, la autoridad, la población, el territorio y la soberanía.

El estado moderno incorpora la noción de *soberanía*, un concepto revolucionario, tal como señala *Jacques Huntzinger*, quien atribuye el paso histórico de una sociedad desagregada y desmigajada, pero cimentada en la religión, a una sociedad de estados organizados e independientes unos de otros.

Pero, este estado moderno, surgido de la aspiración de los reyes a desembarazarse de los lazos feudales y de la jerarquía eclesiástica, el estado – nación, la unión de un poder central, un territorio y una población alrededor del concepto revolucionario de la soberanía, habría de conocer dos formas, dos definiciones diferentes, la primera, el estado principesco y la segunda, el estado democrático.

El estado principesco, se caracterizó por el poder personal ejercido uniformemente sobre un territorio estrictamente delimitado. El príncipe era el soberano, con atribuciones internas y externas. Dentro de su territorio, cobraba impuestos y producía leyes de carácter general, aplicadas coercitivamente, mediante el monopolio de la fuerza pública. Internacionalmente, representaba y obligaba a su Estado.

Y el estado democrático, surgido de las revoluciones inglesa, norteamericana y francesa, trasladó la soberanía del príncipe a la nación. Sus poderes fueron asumidos por organismos surgidos de consultas a la población, mediante reglas de juego previa y claramente definidas. Y al igual que en las *polis* griegas, el sentimiento patriótico se desarrolló y con él los de pertenencia, civismo e interés nacional.


Estado, Nación y Gobierno
-------------------------

La idea de *Estado* No debe confundirse con el concepto de *gobierno*, que sería sólo la parte generalmente encargada de llevar a cabo las funciones del Estado delegando en otras instituciones sus capacidades. El Gobierno también puede ser considerado como el conjunto de gobernantes que, temporalmente, ejercen cargos durante un período limitado dentro del conjunto del Estado.

Tampoco equivale totalmente al concepto, de carácter más ideológico, de *Nación*, puesto que se considera posible la existencia de naciones sin Estado y la posibilidad de que diferentes naciones o nacionalidades se agrupen en torno a un solo Estado.


Administración Pública
----------------------

La *Administración Pública* es un sistema de límites imprecisos que comprende el conjunto de organizaciones públicas que realizan la función administrativa y de gestión del Estado y de otros entes públicos con personalidad jurídica, ya sean de ámbito regional o local.

Por su función, la *Administración Pública* pone en contacto directo a la ciudadanía con el poder político, satisfaciendo los intereses públicos de forma inmediata, por contraste con los poderes legislativo y judicial, que lo hacen de forma mediata.

La palabra *administrar* proviene del latín *ad-ministrare, ad (ir, hacia)* y *ministrare (servir, cuidar)* y tiene relación con la actividad de los ministros romanos en la antigüedad. No obstante, el concepto de *Administración Pública* puede ser entendido desde dos puntos de vista.

Desde un punto de vista formal, se entiende a la entidad que administra, o sea al organismo público que ha recibido del poder político la competencia y los medios necesarios para la satisfacción de los intereses generales.

Desde un punto de vista material, se entiende más bien la actividad administrativa, o sea la actividad de este organismo considerado en sus problemas de gestión y de existencia propia, tanto en sus relaciones con otros organismos semejantes como con los particulares para asegurar la ejecución de su misión.

También se puede entender como la disciplina encargada del manejo científico de los recursos y de la dirección del trabajo humano enfocada a la satisfacción del interés público, entendido este último como las expectativas de la colectividad.

La administración pública, en tanto estructura orgánica, es una creación del Estado, regulada por el derecho positivo y como actividad constituye una función pública establecida por el ordenamiento jurídico nacional. Pero tanto la organización como la función o actividad reúnen, además, caracteres tecnico-políticos, correspondientes a otros campos de estudio no jurídicos, como los de la teoría de la organización administrativa y la ciencia política. Por lo tanto la noción de la administración pública dependerá de la disciplina o enfoques principales de estudio (el jurídico, el técnico o el político), en virtud de no existir una ciencia general de la administración pública capaz de armonizar y fundir todos los elementos y enfoques de este complejo objeto del conocimiento.

La administración pública es la organización que tiene a su cargo la dirección y la gestión de los negocios estatales ordinarios dentro del marco de derecho, las exigencias de la técnica y una orientación política.

La administración pública está caracterizada por atributos propiamente estatales. Dicha administración, por principio, es una cualidad del Estado y solo se puede explicar a partir del Estado. Tal aseveración es aplicable a todas las organizaciones de dominación que se han sucedido en la historia de la humanidad, pero para nuestro caso, es suficiente con ceñirnos al Estado tal y como lo denominó Maquiavelo tiempo atrás: 

.. class:: blockquote

«Los estados y soberanías que han existido y tienen autoridad sobre los hombres, fueron y son, o repúblicas o principados.»


Administración pública en la Argentina
--------------------------------------

La Administración Pública es el conjunto de organismos estatales que realizan las funciones administrativas del Estado argentino. En general abarca a los distintos entes y dependencias que integran el *Poder Ejecutivo Nacional (PEN)*, y los *poderes ejecutivos* provinciales y de la Ciudad Autónoma de Buenos Aires, así como las administraciones municipales.

La Administración Pública no incluye el Poder Legislativo ni el Poder Judicial. Tampoco abarca las empresas estatales ni entes privados que prestan servicios públicos. Incluye en cambio a las entidades públicas descentralizadas y las especializadas, como los centros de enseñanza, hospitales y museos. En principio, las Fuerzas Armadas integran la Administración Pública, aunque poseen un régimen especial.

La Administración Pública actúa mediante actos administrativos y puede ser controlada internamente por los habitantes mediante recursos administrativos regulados por el Derecho Administrativo (procedimiento administrativo), en una primera instancia, o por demanda judicial contra el Estado (procedimiento contencioso-administrativo), en caso de rechazo del recurso.

Los gastos de la Administración Pública se rigen por pautas estrictas establecidas en el presupuesto, aprobado por ley, y por las reglas establecidas en la Ley de Presupuesto de cada jurisdicción. La realización de gastos incumpliendo estas reglas constituye un delito contra la Administración Pública, establecidos en el Código Penal.

El personal de la Administración Pública está regido por reglas especiales, diferentes de las que regulan a los trabajadores de la actividad privada.

En general tienen leyes especiales que contemplan sistemas de ingreso por concurso, prohibición del despedido sin causa justa ni sumario previo. En algunos casos se han establecido sistemas de negociación colectiva.

Esquema de la Administración Pública Argentina
++++++++++++++++++++++++++++++++++++++++++++++

Debido al sistema de organización federal adoptado por la Argentina, el Estado argentino está formado por dos grandes estructuras estatales paralelas: el estado federal (o nacional) y los estados provinciales y la Ciudad Autónoma de Buenos Aires. Cada una de estas estructuras tiene su propia administración pública, conviviendo así la administración pública nacional, con cada una de las administraciones públicas provinciales y de la Ciudad de Buenos Aires. A ellas hay que agregar los gobiernos municipales, que poseen autonomía administrativa en virtud del Artículo 123 de la Constitución Nacional.

El grueso de la Administración Pública en Argentina se encuentra desconcentrada en las administraciones públicas provinciales, donde se encuentra empleado más de la mitad de los empleados públicos del país.

Entre los sectores que administran las provincias se encuentran las escuelas públicas, la policía y los centros de salud, responsables por sí mismos de la mayor parte del gasto y del empleo público.

Luego de las administraciones provinciales, los otros sectores importantes son la Administración Pública Nacional, las administraciones municipales y las universidades nacionales.


Estado Red
----------

A lo largo de los últimos años, los países de la región desarrollaron estrategias y planes de modernización incorporando las tecnologías de la información y las comunicaciones de manera intensiva con el propósito de mejorar la calidad del servicio público y facilitar el acceso a la administración pública por parte de la comunidad en la posibilidad de realizar trámites o incluso en plataformas virtuales de participación democrática. Las políticas públicas definidas están orientadas a superar la brecha digital y aumentar la efectividad de la gestión administrativa.

Manuel Castells [#]_ define:

.. [#] Wikipedia. `Manuel Castells`_

.. _Manuel Castells: https://es.wikipedia.org/wiki/Manuel_Castells

.. class :: blockquote

«El Estado red es el Estado de la era de la información, la forma política que permite la gestión cotidiana de la tensión entre lo local y lo global ... es el principal instrumento del que hoy por hoy disponen los ciudadanos para controlar la globalización en función de sus valores e intereses. Por ello la adecuación del instrumento, la administración del Estado, a las tareas complejas que requiere el extraordinario proceso de cambio social y tecnológico que vivimos, es la condición previa a cualquier capacidad de intervención estratégica pública, a cualquier reforma social.» [#]_

.. [#] Castells, M. (1998) «Hacia el estado red? Globalización económica e instituciones políticas en la era de la información». Ponencia presentada en el Seminario sobre «Sociedad y reforma del estado», organizado por el Ministerio de Administracao Federal e Reforma Do Estado, Republica Federativa do Brasil.

Este concepto lo fundamenta sintetizando la construcción del *Estado-red* como un fenómeno que se da en torno a la combinación de ocho principios de funcionamiento de la administración pública:

Subsidiariedad
  La gestión debe situarse, para cada problema o tarea, en el ámbito más descentralizado posible en donde pueda desempeñarse eficazmente.

Flexibilidad
  Las estructuras organizacionales deben tener la capacidad de adaptarse a las necesidades de intervención del Estado.

Coordinación
  Trabajo estratégico en cuanto a la capacidad de cooperación entre los diversos componentes del Estado.

Participación ciudadana
  Garantizar la legitimidad en los procesos de toma de decisión pública al ponerlos bajo la auditoría de la ciudadanía.

Transparencia administrativa
  Garantizar el acceso a la información por parte de la ciudadanía reforzado con la implementación de los avances tecnológicos.

Modernización tecnológica
  Mediante el uso constante de las redes informáticas y de las comunicaciones.

Transformación de los agentes de la administración
  Con políticas de profesionalización, jerarquización y revalorización de los trabajadores de la administración pública.

Retroacción en la gestión
  Utilizado como mecanismo de autoaprendizaje al interior de las organizaciones.

En definitiva, el *Estado-Red* es la forma de supervivencia del Estado en la era de la información y la globalización. La administración flexible y conectada es el instrumento indispensable.

.. raw:: pdf

    PageBreak

Soberanía tecnológica
=====================

Ser soberano, en principio, significa contar con un poder superior a cualquier otro en el sentido en que nada está por encima de ese poder. En las democracias el poder se encuentra en manos del pueblo (o de sus representantes, a quienes se lo delega) y la soberanía se presenta como una condición de los propios Estados.

Sin embargo, el concepto de soberanía es dinámico y sus distintos sentidos fueron desarrollándose en torno a diferentes aspectos a lo largo de la historia, conformando un proceso de construcción de soberanías. Aquella soberanía territorial de inicios del siglo XIX, necesaria en los orígenes de la construcción del Estado, fue con el tiempo y el avance de las sociedades y sus vínculos con otras, dando lugar a la soberanía como una ampliación de los derechos de los propios pueblos.

Respecto de la *soberanía tecnológica*, entendemos que puede definirse desde dos perspectivas: una positiva, relacionada con la capacidad de los Estados, y otra contraria al concepto de neutralidad tecnológica.

Por la positiva, la soberanía tecnológica se presenta como la posibilidad (o la obligación) del Estado de tener pleno control de la tecnología que utiliza. Esto es de especial interés en el contexto actual del capitalismo informacional contemporáneo, en el cual el software, en tanto herramienta de gestión de datos e información, se vincula no sólo a las diferentes industrias, sino también a la mayoría de las prácticas relacionadas con los mecanismos actuales de comunicación.

En relación a esto último, la soberanía tecnológica implica además la capacidad de conocer qué hace el software que el Estado utiliza en los procesos de administración y manejo de datos, y cómo a través de este control defiende los derechos de los ciudadanos frente a otros derechos, por ejemplo, los económicos de las corporaciones o los intereses de otros Estados.

Sin embargo, también existe el abordaje de la soberanía tecnológica por oposición a otro concepto cuyo sentido fue mutando según los intereses de quienes se lo apropiaban: el de neutralidad tecnológica.

Mal entendido, el principio de neutralidad en la adopción de la tecnología de parte de los Estados, confunde adrede el derecho a competir entre firmas que ofrecen la misma tecnología sin favorecer a ninguna, con las condiciones de la adquisición de software según las necesidades del propio Estado.

Si lo que diferencia al software libre del que no lo es son las condiciones y los derechos del usuario sobre su utilización, lo que está en juego es una decisión que no es tecnológica sino política. En el caso del software, cuando los Estados confunden la tecnología con los requerimientos sobre los programas, tales como el acceso al código fuente y su modificación que habilite, a su vez, independencia de proveedores; este deja en manos del *lobby* del más fuerte la decisión política por la que debiera responder en favor de los ciudadanos y sus derechos.

Situaciones como esta recuerdan que la tecnología no puede entenderse como neutral, porque su aplicación siempre se desarrolla en el marco de relaciones de poder y de intereses que atraviesan los escenarios de su utilización.

Aportes del software libre a la soberanía tecnológica
-----------------------------------------------------

Desde la primera dimensión (sus libertades), el software libre promueve el conocimiento total del funcionamiento e intervención del software y permite el máximo de funcionalidad y seguridad; a la vez que ofrece la posibilidad de mejorarlo continuamente aumentando su calidad y garantizando la perennidad de la información que administra. Es por ello que, desde las necesidades de seguridad nacional y cuidado en el manejo de los datos de los ciudadanos, las características propias de este software son esenciales para que el Estado pueda cumplir con sus funciones sin arriesgarse a los accesos indeseados a datos confidenciales, la manipulación de datos por terceros o la imposibilidad de acceso a la información.

Desde la dimensión socio-política, el software libre ofrece una alternativa a favor de la soberanía e independencia tecnológica de los países de la periferia que, como la Argentina, deben asegurar su autonomía en materia de información a través de la construcción de respuestas locales a problemas locales. La libre circulación del conocimiento y los derechos humanos asociados al acceso a la cultura, son la base de su potencial emancipador.

Desde su dimensión económica, encontramos la reutilización del código fuente disponible como una característica que incrementa la productividad del trabajo y la sustentabilidad de su propuesta. Por otro lado, la forma de producir valor apunta a una redistribución descentralizada de unidades productivas que pueden trabajar el mismo *producto* y, por ende, no sólo permite equilibrar los niveles de ingresos del sector entre proveedores, sino también evitar la formación de monopolios. Este último punto es esencial para el desarrollo local a partir de la incorporación de firmas y profesionales del país. Tal situación alcanzada por el acceso al código fuente –imprescindible para el cumplimiento de las cuatro libertades del software libre– permitiría también la competencia entre firmas en pos de mejoras en la calidad, incluso en el marco de la competitividad internacional que podría evitar, por ende, la fuga de divisas al comprar licencias de uso foráneas en moneda extranjera.

Si se entienden las soberanías actuales como una ampliación de derechos de los pueblos, la elección del software libre en los Estados permite responder favorablemente con la demanda de los derechos de acceso a la información, a la cultura, a la libre expresión, a la educación y a la inclusión de parte de los ciudadanos. Estos derechos se encuentran enmarcados en la Declaración Universal de los Derechos Humanos que contempla tanto los derechos civiles y políticos de 1966 como los indicados en el Pacto Internacional de Derechos Económicos, Sociales y Culturales  del mismo año; indivisibles y con jerarquía constitucional en la Argentina.

El uso de Software Libre asegura la soberanía tecnológica, impulsa la innovación nacional, optimiza el gasto estatal fortaleciendo el desarrollo local y facilita la inclusión digital permitiendo avanzar en objetivos como:

Autonomía tecnológica
  Adoptando Software Libre y con las posibilidades que éste ofrece de acceder al código fuente, muchos usuarios tendrán la posibilidad de pasar de ser consumidores a ser desarrolladores de software. Esto significa que se podrán adaptar los programas a las necesidades específicas de las distintas dependencias, y todas esas modificaciones deberán realizarse siguiendo los requisitos exigidos por el modelo del Software Libre.

Estandarización e Integración
  El Software Libre es producido utilizando especificaciones y estándares tecnológicos libres y públicos, también denominados *estándares abiertos*. Esto beneficia la integración de sistemas y el intercambio de información, de forma que se garantiza la accesibilidad sin restricciones por parte de la ciudadanía.

Seguridad
  El hecho de hacer público el código fuente de los programas favorece a la seguridad de los mismos. Utilizando Software Libre se puede saber qué está haciendo realmente un programa, qué tipo de información maneja y cómo lo hace. Una buena seguridad debe basarse en la transparencia. El software privativo oculta estos aspectos y muchas veces no se conoce si la información está siendo enviada a otras computadoras remotas.

Independencia de proveedores
  Adquiriendo un software privativo se genera una relación de dependencia con respecto a un fabricante, quedando a merced de la evolución que el dueño del software defina para el mismo en cuanto a mantenimiento, actualizaciones y pedidos de funcionalidades.

Democratización de la información
  Las tecnologías de la información han pasado a ocupar un lugar central en la sociedad. Si bien cada vez son más los usuarios que acceden a dichas tecnologías, la *brecha tecnológica* todavía es grande y es un factor más de exclusión social. El Software Libre favorece la democratización de la información.



.. Cierre de apunte con bibliografía

.. raw:: pdf

    PageBreak

Bibliografía
============

* Wikipedia. `Estado. <https://es.wikipedia.org/wiki/Estado>`_

.. raw:: pdf

    Spacer 0 10
    
* Wikipedia. `Administración Pública. <https://es.wikipedia.org/wiki/Administraci%C3%B3n_p%C3%BAblica>`_

.. raw:: pdf

    Spacer 0 10
    
* Xhardez, Verónica. `Contribuciones del software libre a la soberanía tecnológica y los desafíos futuros. <http://www.vocesenelfenix.com/content/contribuciones-del-software-libre-la-soberan%C3%ADa-tecnol%C3%B3gica-y-los-desaf%C3%ADos-futuros>`_


